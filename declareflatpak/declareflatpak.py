import foxcommon as fc
import subprocess


def configure():
    pass


def plymouth_init():
    fc.execute("plymouth change-mode --updates")
    fc.execute("plymouth update --status=Installing recommended flatpaks")
    

def plymouth_percent(percent):
    fc.execute(f"plymouth system-update --progress={percent}")


def plymouth_exit():
    fc.execute("plymouth system-update --progress=100")
    fc.execute("plymouth change-mode --boot-up")


def install():
    plymouth_init()

    with open("/etc/declare/flatpak", "r") as flatpaks:
        flatpaks = [flatpak.strip() for flatpak in flatpaks.read().split(" ")]

        percent = 0
        plymouth_percent(percent)

        for i, flatpak in enumerate(flatpaks):
            fc.info(f"Installing flatpak {flatpak}")
            subprocess.run(
                f"flatpak install --noninteractive -y {flatpak}",
                shell=True,
                check=True,
            )
            percent = round((i+1)/len(flatpaks)*100)
            plymouth_percent(percent)

    fc.execute("systemctl disable declareflatpak.service") # TODO: add check for openrc and do equivalent command
    fc.execute("rc-update del declareflatpak")

    plymouth_exit()


def main():
    install()


if __name__ == "__main__":
    main()
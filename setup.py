#!/usr/bin/env python3

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

name = "declareflatpak"
description = "foxdeclare module for flatpaks."

setup(
    name = f"{name}",
    version = "1.0.0",
    author = "Luna Deards",
    author_email = "luna@xenialinux.com",
    description= (description),
    license = "GPL-3",
    keywords = "update utility",
    url = f"https://gitlab.com/xenia-group/{name}",
    packages = [f"{name}"],
    requires=["foxcommon"],
    entry_points={
        'console_scripts': [
            f'declare-flatpak = {name}.{name}:main',
        ],
    },
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Package"
        "License :: OSI Approved :: GPL-3"
    ],
)
